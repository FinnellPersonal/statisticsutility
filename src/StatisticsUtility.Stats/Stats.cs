﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace StatisticsUtility.Stats
{
    public abstract class Statistic {
        public static Dictionary<string, Statistic> stats = new Dictionary<string, Statistic>(StringComparer.InvariantCultureIgnoreCase) {
            { "MinValue", new MinCalculation() },
            { "MaxValue", new MaxCalculation() },
            { "Average",  new AverageCalculation() },
        };

        public abstract decimal Calculate(IEnumerable<decimal> values);
    }

    public class MinCalculation : Statistic {
        public override decimal Calculate(IEnumerable<decimal> values){
            return values.Min();
        }
    }

    public class MaxCalculation : Statistic {
        public override decimal Calculate(IEnumerable<decimal> values){
            return values.Max();
        }
    }

    public class AverageCalculation : Statistic {
        public override decimal Calculate(IEnumerable<decimal> values){
            return values.Average();
        }
    }
}