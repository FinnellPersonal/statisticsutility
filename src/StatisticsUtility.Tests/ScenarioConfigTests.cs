using System;
using NUnit.Framework;
using StatisticsUtility;
using StatisticsUtility.Periods;
using StatisticsUtility.Stats;

namespace ScenarioConfigTests
{
    [TestFixture]
    public class ScenarioTests
    {
        private static string _line = "1\tCashPrem\t1\t2\t3\t4";

        [TestCase("1\tCashPrem\t1\t2\t3\t4")]
        [TestCase("2\tAvePolLoanYield\t1\t2\t3\t4")]
        [TestCase("3\tResvAssumed\t1\t2\t3\t4")]
        public void ScenarioCreation_WithValid_DataRow(string line){
            Scenario scenario = new Scenario(line);

            Assert.IsNotNull(scenario); }

        [Test]
        public void ScenarioCreate_WithInvalid_DataRow(){
            Assert.Catch<FormatException>(() => { 
                string failingLine = "CashPrem\t1\t2\t3\t4";

                var scenario = new Scenario(failingLine); 
            });
        }

        [Test]
        public void ScenarioPeriod_GetFirstValue(){
            Scenario scenario = new Scenario(_line);

            Period period = new FirstValue();

            var value = period.GetValue(scenario.values);

            Assert.AreEqual(value, 1m);
        }

        [Test]
        public void ScenarioPeriod_GetLastValue(){
            Scenario scenario = new Scenario(_line);

            Period period = new LastValue();

            var value = period.GetValue(scenario.values);

            Assert.AreEqual(value, 4m);
        }

        [Test]
        public void ScenarioPeriod_GetMinValue(){
            Scenario scenario = new Scenario(_line);

            Period period = new MinValue();

            var value = period.GetValue(scenario.values);

            Assert.AreEqual(value, 1m);
        }

        [Test]
        public void ScenarioPeriod_GetMaxValue(){
            Scenario scenario = new Scenario(_line);

            Period period = new MaxValue();

            var value = period.GetValue(scenario.values);

            Assert.AreEqual(value, 4m);
        }

        [TestCase("firstValue")]
        [TestCase("fiRstValue")]
        [TestCase("FIRSTVALUE")]
        [TestCase("firstvalue")]
        public void Period_Tolerate_Spelling(string periodName){
            Period resultPeriod = Period.values[periodName];

            Assert.IsInstanceOf<FirstValue>(resultPeriod);
        }
    }

    [TestFixture]
    public class ConfigTests
    {
        private string _line = "CashPrem\tAverage\tMaxValue";

        [Test]
        public void ConfigItem_Create_ValidLine(){
            ConfigItem ci = new ConfigItem(_line);

            Assert.AreEqual(ci.variable, "CashPrem");

            // Make sure proper types are instantiated
            Assert.IsInstanceOf<AverageCalculation>(ci.stat);
            Assert.IsInstanceOf<MaxValue>(ci.period);
        }

        [TestCase("")]
        [TestCase("\t\t\t")]
        [TestCase("CashPrem Average MaxValue")]
        [TestCase("CashPrem\tAverage")]
        [TestCase("CashPrem\tNotAStat\tMaxValue")]
        [TestCase("CashPrem\tAverage\tNotAPeriod")]
        public void ConfigItem_Create_InvalidLine(string line){
            Assert.Catch<Exception>(() => {
                var ci = new ConfigItem(line); 
            });
        }
    }
}