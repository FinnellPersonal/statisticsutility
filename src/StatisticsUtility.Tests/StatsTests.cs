using System;
using System.Collections.Generic;
using NUnit.Framework;
using StatisticsUtility;
using StatisticsUtility.Stats;

namespace StatsTests
{
    [TestFixture]
    public class StatsTests
    {
        private static List<decimal> _values = new List<decimal>(){
            1m, 2m, 3m, 4m, 5m
        };

        [TestCase("MinValue")]
        [TestCase("MaxValue")]
        [TestCase("Average")]
        public void Statistic_GetFromString(string name){
            Assert.IsInstanceOf<Statistic>(Statistic.stats[name]);
        }

        [TestCase("")]
        [TestCase("NotAStatistic")]
        public void Statistic_GetFromInvalidString_Break(string name){
            Assert.Catch<Exception>(() => {
                var stat = Statistic.stats[name];
            });
        }

        [TestCase("minvalue")]
        [TestCase("MINVALUE")]
        [TestCase("MInVALuE")]
        [TestCase("MinvaluE")]
        public void Statistic_Tolerate_Spelling(string name) {
            Statistic stat = new MinCalculation();

            Assert.IsInstanceOf<MinCalculation>(stat);
        }

        [Test]
        public void Statistic_Calculate_MinValue(){
            Statistic stat = new MinCalculation();

            var result = stat.Calculate(_values);

            Assert.AreEqual(result, 1m);
        }

        [Test]
        public void Statistic_Calculate_MaxValue(){
            Statistic stat = new MaxCalculation();

            var result = stat.Calculate(_values);

            Assert.AreEqual(result, 5m);
        }

        [Test]
        public void Statistic_Calculate_Average(){
            Statistic stat = new AverageCalculation();

            var result = stat.Calculate(_values);

            Assert.AreEqual(result, 3m);
        }
    }
}