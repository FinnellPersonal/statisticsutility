﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using StatisticsUtility.Periods;
using StatisticsUtility.Stats;

namespace StatisticsUtility
{
    class StatisticsUtility
    {
        static void Main(string[] args)
        {
            // Parse Configuration
            var config = new Config("Configuration.txt");

            // Init dictionary of <config items, values for stat computation (as defined by config item)>
            Dictionary<ConfigItem, List<Decimal>> valueTable = new Dictionary<ConfigItem, List<decimal>>(); 
            foreach(ConfigItem configItem in config.configItems){
                valueTable.Add(configItem, new List<decimal>());
            }

            // Iterate through Data file, line per line. Select periods for later statistic calculations
            foreach(String line in File.ReadLines("TotalTemp.txt").Skip(1)) {
                // Map file line to scenario object
                Scenario scenario = new Scenario(line);

                foreach(ConfigItem configItem in config.configItems){
                    if(string.Equals(scenario.variable, configItem.variable)){
                        decimal value = configItem.period.GetValue(scenario.values);

                        valueTable[configItem].Add(value);
                    }
                }
            }

            // Calculate Statistics and Write to File
            using (var file = new System.IO.StreamWriter("Output.txt")){
                foreach(var entry in valueTable) {
                    ConfigItem configItem = entry.Key;
                    List<decimal> values  = entry.Value;

                    // Calculate statistic
                    decimal result = configItem.stat.Calculate(values);

                    // Write to output file
                    file.WriteLine("{0}\t{1}\t{2:0.00}", configItem.variable, configItem.stat, result);
                }
            }
        }
    }

    // Represents a row in the TotalTemp data file
    public class Scenario {
        public long scenId;
        public string variable;
        public IEnumerable<decimal> values;

        // Assumes data of form <Id, Variable, [values]>
        public Scenario(string line){
            var items = line.Split('\t');

            // [0] Id as first element
            scenId = Int64.Parse(items[0]);

            // [1] variable as second element
            variable = items[1];

            // [2:N] values as remaining elements
            values = items
                .Skip(2)
                .Select(item => decimal.Parse(item));
        }
    }

    // Config file in object form <VariableName, Statistic, PeriodChoice>
    public class Config {
        public List<ConfigItem> configItems;

        public Config(string fileName){
            configItems = new List<ConfigItem>();

            foreach(string line in File.ReadLines(fileName)){
                configItems.Add(new ConfigItem(line));
            }
        }
    }

    public class ConfigItem {

        public string variable;
        public Statistic stat;
        public Period period;

        public ConfigItem(string line){
            string[] items = line.Split('\t');

            variable = items[0];

            // get stat and period objects from static <string, object> mapping tables
            stat     = Statistic.stats[items[1]];
            period   = Period.values[items[2]];
        }
    }
}