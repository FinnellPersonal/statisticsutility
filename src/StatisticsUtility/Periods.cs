using System;
using System.Collections.Generic;
using System.Linq;

namespace StatisticsUtility.Periods {
    public abstract class Period {
        public static Dictionary<string, Period> values = new Dictionary<string, Period>(StringComparer.InvariantCultureIgnoreCase){
            { "FirstValue", new FirstValue() },
            { "LastValue",  new LastValue() },
            { "MaxValue",   new MaxValue() },
            { "MinValue",   new MinValue() },
        };

        public abstract decimal GetValue(IEnumerable<decimal> values);
    }

    public class FirstValue : Period {
        public override decimal GetValue(IEnumerable<decimal> values){
            return values.First();
        }
    }

    public class LastValue : Period {
        public override decimal GetValue(IEnumerable<decimal> values){
            return values.Last();
        }
    }

    public class MinValue : Period {
        public override decimal GetValue(IEnumerable<decimal> values){
            return values.Min();
        }
    }

    public class MaxValue : Period {
        public override decimal GetValue(IEnumerable<decimal> values){
            return values.Max();
        }
    }
}