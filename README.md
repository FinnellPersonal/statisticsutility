# Milliman Statistics Utility

## Usage

```sh
# Obtain code
git clone https://MattFinnell@bitbucket.org/FinnellPersonal/statisticsutility.git

# Move into main project directory
cd statisticsutility/src/StatisticsUtility

# Run project
dotnet run

# Print Output.txt file
cat Output.txt
```

**NOTE** - This currently considers that `Configuration.txt` and `TotalTemp.txt` files are located within `statisticsutility/src/StatisticsUtility` and will output `Output.txt` in the same directory. I have yet to add CLI arguments to make this behavior more flexible.

## Testing
```sh
dotnet test statisticsutility/src/StatisticsUtility.Tests/StatisticsUtility.Tests.csproj
```

## Development Environment

- OSX 10.13
- Microsoft `.NET Core v2.0.3`
- VSCode
- zsh shell

## Staging Environment (sanity check)

- Ubuntu 16.04 (`ubuntu/xenial64` vagrant image) via Virtualbox
- Provisioned .NET Core via [these steps](https://www.microsoft.com/net/learn/get-started/linuxubuntu).

```sh
# Register microsoft signature key
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg

# Add microsoft packages to package manager list
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-xenial-prod xenial main" > /etc/apt/sources.list.d/dotnetdev.list'

# Update package manager
sudo apt-get update

# Install .NET core
sudo apt-get install dotnet-sdk-2.0.2
```

## Todo
- Implement CLI arguments for `config`, `tempdata`, and `output` file paths.
- ~~Process scenario values *after* filtering.~~
- Raise Exceptions for bad input
- Process header of tempdata file.
- Tighten up code conventions.
- Implement strong tests.
- Export and package as CLI application.
- Coverage Reporting.
- Could be containerized instead of using bulky vagrant images.

## Notes

- IEnumerables are the primary iterable in this project. This was decided because of their *On request* nature so that
the entire text file doesn't get loaded into memory.
- I implemented basic functionality first before testing so that I could understand the shape of the implementation.
- I have this repository linked with a continuous integration server (Circle CI) for when I
start implementing stronger logic. This is after I lock down the minimal project with unit tests.
